package pt.com.scrumify.configuration;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.springframework.security.test.context.support.WithSecurityContext;

@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = MockScrumifyUserWithSecurityContextFactory.class)
public @interface MockScrumifyUser {
   String username() default "admin";
}