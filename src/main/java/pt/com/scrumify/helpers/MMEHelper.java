package pt.com.scrumify.helpers;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.services.ContractService;
import pt.com.scrumify.entities.UploadInfo;
import pt.com.scrumify.helpers.ConstantsHelper.TYPES_OF_MME_REPORT;
import pt.com.scrumify.services.MMEService;

@Service
public class MMEHelper {

   private static final Logger logger = LoggerFactory.getLogger(MMEHelper.class);

   private static final String AMENDMENT_EXTENSION = "Amendment/Extension";
   private static final String CONTRACT_NUMBER = "Contract Number";
   private static final String NO = "no";
   private static final String UPLOAD_MESSAGE_ERROR = "Error occured during processing";
   private static final String UPLOAD_MESSAGE_ERROR_CONTRACT = "Contract not found";
   private static final String UPLOAD_MESSAGE_ERROR_UNKOWN_TYPE = "Unknown report type";
   private static final String UPLOAD_MESSAGE_SUCCESS = "Success";
   
   
   @Autowired
   private ContractService contractService;
   @Autowired
   private MMEService service;
      
   public List<UploadInfo> processReport(MultipartFile file) throws IOException {
      List<UploadInfo> infos = new ArrayList<>();
      long start = System.nanoTime();
      String filename = file.getOriginalFilename();
      logger.info("Reading file {}.", filename);
      UploadInfo info = new UploadInfo(filename, file.getSize()/1024);

      //
      // Detect report type
      //
      ConstantsHelper.TYPES_OF_MME_REPORT reportType = detectReportType(file);
      logger.info("Processing report {}.", reportType.getDescription());  
      
      //
      // Open workbook
      //
      try (Workbook workbook = new XSSFWorkbook(file.getInputStream())) {
         String contractNumber = getContractNumber(workbook, reportType); 
         logger.info("Contract Number: {}", contractNumber);

         Contract contract = contractService.getByTwoPropertyNameAndValue(CONTRACT_NUMBER, contractNumber, AMENDMENT_EXTENSION, NO);
         if (contract != null) {
            switch (reportType) {
               case POC_DETAILS:
                  service.processPoCDetails(workbook, contract, info);
                  break;
               case RESOURCES_TREND:
                  service.processResourceTrend(workbook, contract, info);
                  break;
               case SOLUTION_CONTINGENCY:
                  service.processSolutionContigency(workbook, contract, info);
                  break;
               default:
                  info.setMessage(UPLOAD_MESSAGE_ERROR_UNKOWN_TYPE);
                  logger.error("Unknown report type.");
                  break;
            }
         }
         else {
            info.setMessage(UPLOAD_MESSAGE_ERROR_CONTRACT);
            logger.error("Contract not found for {} report.", reportType.getDescription());
         }
      }
      catch (Exception ex) {
         info.setMessage(UPLOAD_MESSAGE_ERROR);
         logger.error("An error has occurred processing {} report. Exception: {}", reportType.getDescription(), ex.getMessage());
      }
      
      double end = Double.valueOf((System.nanoTime() - start)) / 1000000000;
      logger.info("{} report finished ({} seconds).", reportType.getDescription(), end);
      info.setTime(end);
      
      if(info.getMessage().isEmpty())
         info.setMessage(UPLOAD_MESSAGE_SUCCESS);
      
      infos.add(info);
      return infos;
   }

   private String getContractNumber(Workbook workbook, TYPES_OF_MME_REPORT reportType) {
      String contractNumber = "";
      
      switch (reportType) {
         case POC_DETAILS:
            contractNumber = parseCellAsString(workbook, ConstantsHelper.POC_DETAILS_SHEET_DATA, ConstantsHelper.EXCEL_ROW_8, ConstantsHelper.EXCEL_COLUMN_C, ConstantsHelper.EXCEL_STRING_FORMAT);
            break;
         case RESOURCES_TREND:
            contractNumber = parseCellAsString(workbook, ConstantsHelper.RESOURCES_TREND_SHEET_INFO, ConstantsHelper.EXCEL_ROW_6, ConstantsHelper.EXCEL_COLUMN_C, ConstantsHelper.EXCEL_DECIMAL_10_0_FORMAT);
            break;
         case SOLUTION_CONTINGENCY:
            contractNumber = parseCellAsString(workbook, ConstantsHelper.SOLUTION_CONTINGENCY_SHEET_INFO, ConstantsHelper.EXCEL_ROW_7, ConstantsHelper.EXCEL_COLUMN_C, ConstantsHelper.EXCEL_DECIMAL_10_0_FORMAT);
            break;
         default:
            break;
      }
      
      return contractNumber;
   }
   
   private TYPES_OF_MME_REPORT detectReportType(MultipartFile file) throws IOException {
      TYPES_OF_MME_REPORT reportType = TYPES_OF_MME_REPORT.UNKNOWN;
      
      try (Workbook workbook = new XSSFWorkbook(file.getInputStream())) {
         String b1 = workbook.getSheetAt(0).getRow(0).getCell(1).getRichStringCellValue().getString().trim();
         String b2 = workbook.getSheetAt(0).getRow(1).getCell(1).getRichStringCellValue().getString().trim();
         
         if (b2.equalsIgnoreCase(ConstantsHelper.TYPES_OF_MME_REPORT.POC_DETAILS.getDescription())) {
            reportType = TYPES_OF_MME_REPORT.POC_DETAILS;
         }
         else if (b1.equalsIgnoreCase(ConstantsHelper.TYPES_OF_MME_REPORT.RESOURCES_TREND.getDescription())) {
            reportType = TYPES_OF_MME_REPORT.RESOURCES_TREND;
         }
         else if (b2.equalsIgnoreCase(ConstantsHelper.TYPES_OF_MME_REPORT.SOLUTION_CONTINGENCY.getDescription())) {
            reportType = TYPES_OF_MME_REPORT.SOLUTION_CONTINGENCY;
         }
      }
      
      return reportType;
   }
   
   public BigDecimal parseCellAsBigDecimal(Row row, int cellnum) {
      BigDecimal result = BigDecimal.ZERO;
      
      try {
         String value = parseCellAsString(row, cellnum, ConstantsHelper.EXCEL_DECIMAL_18_6_FORMAT);
         result = value.isEmpty() ? BigDecimal.ZERO : BigDecimal.valueOf(Double.parseDouble(value));
      }
      catch (Exception ex) {
         logger.error("An error has occured parsing cell to String. Exception: {}", ex.getMessage());
      }
      
      return result;
   }
   
   public BigDecimal parseCellAsBigDecimal(Workbook workbook, String sheetname, int rownum, int cellnum) {
      BigDecimal result = BigDecimal.ZERO;

      try {
         Row row = getWorkbookRow(workbook, sheetname, rownum);
         result = parseCellAsBigDecimal(row, cellnum);
      }
      catch (Exception ex) {
         logger.error("An error has occured parsing cell to BigDecimal. Exception: {}", ex.getMessage());
      }
      
      return result;
   }
   
   public Date parseCellAsDate(Row row, int cellnum, String format) {
      Date result = null;
      
      try {
         SimpleDateFormat formatter = new SimpleDateFormat(format);
         Cell cell = getWorkbookCell(row, cellnum);
         String value = cell != null ? cell.toString() : "";
         
         if (!value.isEmpty()) {
            result = formatter.parse(value);
         }
      }
      catch (Exception ex) {
         logger.error("An error has occured parsing cell to date. Exception: {}", ex.getMessage());
      }
      
      return result;
   }

   public String parseCellAsString(Workbook workbook, String sheetname, int rownum, int cellnum, String format) {
      Cell cell = getWorkbookCell(workbook, sheetname, rownum, cellnum);
      
      return parseCellAsString(cell, format);
   }
   
   public String parseCellAsString(Row row, int cellnum, String format) {
      String result = null;
      
      try {
         Cell cell = getWorkbookCell(row, cellnum);
         result = parseCellAsString(cell, format);
      }
      catch (Exception ex) {
         logger.error("An error has occured parsing cell to date. Exception: {}", ex.getMessage());
      }
      
      return result;
   }
   
   public String parseCellAsString(Cell cell, String format) {
      String result = "";

      try {
         switch (cell.getCellType()) {
            case NUMERIC:
               result = String.format(Locale.ENGLISH, format, cell.getNumericCellValue());
               break;
            case STRING:
               result = cell.getStringCellValue();
               break;
            default:
               break;
         }
      }
      catch (Exception ex) {
         logger.error("An error has occured parsing cell to String. Exception: {}", ex.getMessage());
      }
      
      return result.trim().replace("\u00A0", "");
   }
   
   public Row getWorkbookRow(Workbook workbook, String sheetname, int rownum) {
      Row result = null;
      
      try {
         result = workbook.getSheet(sheetname).getRow(rownum);
      }
      catch (Exception ex) {
         logger.error("An error has occurred getting workbook row.");
      }
      
      return result;
   }
   
   public Cell getWorkbookCell(Workbook workbook, String sheetname, int rownum, int cellnum) {
      Cell result = null;
      
      try {
         result = workbook.getSheet(sheetname).getRow(rownum).getCell(cellnum);
      }
      catch (Exception ex) {
         logger.error("An error has occurred getting workbook cell.");
      }
      
      return result;
   }
   
   public Cell getWorkbookCell(Row row, int cellnum) {
      Cell result = null;
      
      try {
         result = row.getCell(cellnum);
      }
      catch (Exception ex) {
         logger.error("An error has occurred getting workbook cell.");
      }
      
      return result;
   }
}