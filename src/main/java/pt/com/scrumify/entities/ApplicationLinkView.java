package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.database.entities.Application;
import pt.com.scrumify.database.entities.ApplicationLink;
import pt.com.scrumify.database.entities.Environment;

@NoArgsConstructor
public class ApplicationLinkView {
   @Getter
   @Setter
   private Integer id;

   @Getter
   @Setter
   private String name;

   @Getter
   @Setter
   private String url;
   
   @Getter
   @Setter
   private String username;
   
   @Getter
   @Setter
   private String password;

   @Getter
   @Setter
   private Application application = new Application();
   
   @Getter
   @Setter
   private Environment environment = new Environment();
   
   public ApplicationLinkView(ApplicationLink link) {
      super();

      this.setId(link.getId());
      this.setApplication(link.getApplication());
      this.setEnvironment(link.getEnvironment());
      this.setName(link.getName());
      this.setUrl(link.getUrl());
      this.setUsername(link.getUsername());
      this.setPassword(link.getPassword());
   }

   public ApplicationLink translate() {
      ApplicationLink link = new ApplicationLink();

      link.setId(this.id);
      link.setApplication(this.application);
      link.setEnvironment(this.environment);
      link.setName(this.name);
      link.setUrl(this.url);
      link.setUsername(this.username);
      link.setPassword(this.password);
      
      return link;  
   }
}