package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.WorkItem;

public class ResourceReport {
   
   @Getter
   @Setter
   private int month;
   
   @Getter
   @Setter
   private Integer hoursSpent = 0;
   
   @Getter
   @Setter
   private int absence= 0;
   
   @Getter
   @Setter
   private int hours= 0;
   
   @Getter
   @Setter
   private WorkItem workitem;
   
   @Getter
   @Setter
   private Contract contract;
   
      
   public ResourceReport() {
      super();
   }
   
   public ResourceReport(int month) {
      super();
      this.month = month;
      this.hoursSpent = 0;
   }
}