package pt.com.scrumify.services;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.TypeOfWorkItem;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.services.TypeOfWorkItemService;
import pt.com.scrumify.database.services.WorkItemService;
import pt.com.scrumify.database.services.WorkItemStatusService;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.WorkItemsHelper;

@Service
public class WorkItemAutomatorService {
   @Autowired
   SecurityService securityService;
   @Autowired
   TypeOfWorkItemService typeOfWorkItemService;
   @Autowired
   WorkItemService workItemService;
   @Autowired
   WorkItemStatusService workItemStatusService;
   @Autowired
   WorkItemsHelper workItemHelper;

   public void run(Resource resource, String action, String description) {
      /*
       * Set security context for current resource
       */
      securityService.setSecurityContext(resource);
      
      if (action.contains("#add-estimate")) {
         this.addEstimate(resource, action);
      }
      if (action.contains("#new-bug")) {
         this.newBug(resource, action, description);
      }
      if (action.contains("#new-incident")) {
         this.newIncident(resource, action, description);
      }
      if (action.contains("#add-request")) {
         this.newRequest(resource, action, description);
      }
      if (action.contains("#new-task")) {
         this.newTask(resource, action, description);
      }
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_UPDATE + "')")
   private void addEstimate(Resource resource, String action) {
      Pattern pattern = Pattern.compile("#add-estimate ([A-Z]{3}[0-9]{8}) (\\d*)");

      Matcher matcher = pattern.matcher(action);
      if (matcher.find()) {
         String number = matcher.group(1);
         int estimate = Integer.parseInt(matcher.group(2));

         WorkItem workitem = workItemService.getByNumber(number);
         if (workitem != null && workitem.getEstimate() == 0) {
            workitem.setEstimate(estimate);
            workitem.setLastUpdate(DatesHelper.now());
            workitem.setLastUpdateBy(resource);
         }

         workItemService.save(workitem);
      }
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_CREATE + "') and " + ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_BUGS + "')")
   private void newBug(Resource resource, String action, String description) {
      Pattern pattern = Pattern.compile("#new-bug (.*)");

      Matcher matcher = pattern.matcher(action);
      if (matcher.find()) {
         String title = matcher.group(1);

         this.newWorkItem(title, description, 0, TypeOfWorkItem.INCIDENT, resource);
      }
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_CREATE + "') and " + ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_INCIDENTS + "')")
   private void newIncident(Resource resource, String action, String description) {
      Pattern pattern = Pattern.compile("#new-incident (.*)");

      Matcher matcher = pattern.matcher(action);
      if (matcher.find()) {
         String title = matcher.group(1);

         this.newWorkItem(title, description, 0, TypeOfWorkItem.INCIDENT, resource);
      }
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_CREATE + "') and " + ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_REQUESTS + "')")
   private void newRequest(Resource resource, String action, String description) {
      Pattern pattern = Pattern.compile("#new-request (.*)");

      Matcher matcher = pattern.matcher(action);
      if (matcher.find()) {
         String title = matcher.group(1);

         this.newWorkItem(title, description, 0, TypeOfWorkItem.INCIDENT, resource);
      }
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_CREATE + "') and " + ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_TASKS + "')")
   private void newTask(Resource resource, String action, String description) {
      Pattern pattern = Pattern.compile("#new-task (.*)");

      Matcher matcher = pattern.matcher(action);
      if (matcher.find()) {
         String title = matcher.group(1);

         this.newWorkItem(title, description, 0, TypeOfWorkItem.TASK, resource);
      }
   }

   @Transactional
   private void newWorkItem(String title, String description, int estimate, int type, Resource resource) {
      TypeOfWorkItem typeOfWorkItem = typeOfWorkItemService.getOne(type);
      
      WorkItem workitem = new WorkItem();
      workitem.setName(title);
      workitem.setType(typeOfWorkItem);
      workitem.setNumber(workItemHelper.formatWorkItemNumber(typeOfWorkItem));
      workitem.setDescription(description);
      workitem.setStatus(workItemStatusService.getOneByTypeOfWorkitem(type));
      if (resource.getTeamMembers() != null) 
         workitem.setTeam(resource.getTeamMembers().get(0).getTeam());
      workitem.setEtc(0);
      workitem.setEstimate(0);

      workItemService.save(workitem);
   }
}