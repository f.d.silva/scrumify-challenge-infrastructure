package pt.com.scrumify.reports;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Epic;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.UserStory;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.database.services.EpicService;
import pt.com.scrumify.database.services.SubAreaService;
import pt.com.scrumify.database.services.TeamService;
import pt.com.scrumify.database.services.UserStoryService;
import pt.com.scrumify.database.services.WorkItemService;
import pt.com.scrumify.entities.ReportView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Service
public class WorkItemsByEpicAndUserStoryExportService extends ExcelReportBase implements ReportExportService {
   @Autowired
   private EpicService epicsService;
   @Autowired
   private SubAreaService subAreasService;
   @Autowired
   private TeamService teamsService;
   @Autowired
   private UserStoryService userStoriesService;
   @Autowired
   private WorkItemService workItemsService;
   
   private static final Logger logger = LoggerFactory.getLogger(BacklogExportService.class);

   private static final String TEMPLATE_PATH = "templates/reports/templates/WorkItemsByEpicAndUserStory_Template.xlsx";
   private static final String WORKSHEET_HOME = "HOME";
   private static final String WORKSHEET_TEMPLATE = "TEMPLATE";
   
   private static final int COLOR_WHITE = 0;
   private static final int COLOR_BLACK = 1;
   private static final int COLOR_GREY_LIGHT = 2;
   private static final int COLOR_GREY = 3;
   private static final int COLOR_BLUE = 4;
   private static final int COLOR_BLUE_DARK = 5;
         
   private static final int FONT_8 = 0;
   private static final int FONT_8_BOLD = 1;
   private static final int FONT_18 = 2;
   private static final int FONT_24 = 3;

   private static final int STYLE_HOME_TITLE = 0;
   private static final int STYLE_HOME_TEXT = 1;
   private static final int STYLE_TABLE_HEADER_ALIGN_LEFT = 2;
   private static final int STYLE_TABLE_HEADER_ALIGN_CENTER = 3;
   private static final int STYLE_TABLE_ROW_ALIGN_LEFT = 4;
   private static final int STYLE_TABLE_ROW_ALIGN_CENTER = 5;
   
   private List<SubArea> subAreas;
   private List<Team> teams;
   
   @Override
   public void addColors() {
      this.addColor((byte)255, (byte)255, (byte)255);
      this.addColor((byte)0, (byte)0, (byte)0);
      this.addColor((byte)242, (byte)242, (byte)242);
      this.addColor((byte)191, (byte)191, (byte)191);
      this.addColor((byte)0, (byte)132, (byte)204);
      this.addColor((byte)0, (byte)28, (byte)68);
   }

   @Override
   public void addFonts() {
      this.addFont(REPORT_FONT_NAME, (short) 8, false, false, colors.get(COLOR_BLACK));
      this.addFont(REPORT_FONT_NAME, (short) 8, true, false, colors.get(COLOR_BLUE));
      this.addFont(REPORT_FONT_NAME, (short) 18, false, false, colors.get(COLOR_BLACK));
      this.addFont(REPORT_FONT_NAME, (short) 24, true, false, colors.get(COLOR_WHITE));
   }

   @Override
   public void addStyles() {
      /*
       * STYLE_HOME_TITLE
       */
      this.addStyle(false, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, FillPatternType.SOLID_FOREGROUND, colors.get(COLOR_BLUE_DARK), fonts.get(FONT_24));
      
      /*
       * STYLE_HOME_TEXT
       */
      this.addStyle(false, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, FillPatternType.SOLID_FOREGROUND, colors.get(COLOR_GREY_LIGHT), fonts.get(FONT_18));
      
      /*
       * STYLE_TABLE_HEADER_ALIGN_LEFT
       */
      this.addStyle(false, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, FillPatternType.SOLID_FOREGROUND, colors.get(COLOR_GREY_LIGHT), fonts.get(FONT_8_BOLD), null, null, null, null, BorderStyle.THIN, colors.get(COLOR_BLUE_DARK), null, null);
      
      /*
       * STYLE_TABLE_HEADER_ALIGN_CENTER
       */
      this.addStyle(false, HorizontalAlignment.CENTER, VerticalAlignment.CENTER, FillPatternType.SOLID_FOREGROUND, colors.get(COLOR_GREY_LIGHT), fonts.get(FONT_8_BOLD), null, null, null, null, BorderStyle.THIN, colors.get(COLOR_BLUE_DARK), null, null);
      
      /*
       * STYLE_TABLE_ROW_ALIGN_LEFT
       */
      this.addStyle(true, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, FillPatternType.SOLID_FOREGROUND, colors.get(COLOR_GREY_LIGHT), fonts.get(FONT_8), null, null, null, null, BorderStyle.DOTTED, colors.get(COLOR_GREY), null, null);
      
      /*
       * STYLE_TABLE_ROW_ALIGN_CENTER
       */
      this.addStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER, FillPatternType.SOLID_FOREGROUND, colors.get(COLOR_GREY_LIGHT), fonts.get(FONT_8), null, null, null, null, BorderStyle.DOTTED, colors.get(COLOR_GREY), null, null);
   }

   @Override
   public void filters(ReportView view) {
      if ((view.getSubAreas() == null || view.getSubAreas().isEmpty()) && (view.getTeams() == null || view.getTeams().isEmpty())) {
         this.subAreas = subAreasService.listSubareasByResource(ResourcesHelper.getResource());
         this.teams = teamsService.getByMember(ResourcesHelper.getResource());
      }
      else if (view.getSubAreas() != null && !view.getSubAreas().isEmpty()) {
         this.subAreas = view.getSubAreas();
         this.teams = teamsService.getBySubAreasAndResource(this.subAreas, ResourcesHelper.getResource());
      }
      else if (view.getTeams() != null && !view.getTeams().isEmpty()) {
         this.teams = view.getTeams();
         this.subAreas = subAreasService.byTeams(this.teams);
      }
   }
   
   @Override
   public ByteArrayInputStream export(ReportView view) {
      ClassPathResource file = new ClassPathResource(TEMPLATE_PATH);
      
      /*
       * Init
       */
      this.init();
      
      /*
       * Open template workbook
       */
      try {
         this.workbook = new XSSFWorkbook(file.getInputStream());
      }
      catch (IOException e) {
         logger.info("#internal.error #exception {}.", e.getMessage());
      }
      
      /*
       * Add colors
       */
      addColors();
      
      /*
       * Add fonts
       */
      addFonts();
      
      /*
       * Add styles
       */
      addStyles();
      
      if (this.workbook != null) {
         /*
          * Get filters
          */
         filters(view);
         
         /*
          * Write homesheet data
          */
         writeHome(view);
         
         /*
          * Write report content
          */
         writeContent(view);
         
         ByteArrayOutputStream output = new ByteArrayOutputStream();

         try {
            this.workbook.write(output);
            this.workbook.close();
         }
         catch (IOException e) {
            logger.info("#internal.error #exception {}.", e.getMessage());
         }
         
         return new ByteArrayInputStream(output.toByteArray());
      }
      
      return null;
   }

   @Override
   public void writeHome(ReportView view) {
      /*
       * WORKSHEET
       */
      this.worksheet = this.workbook.getSheet(WORKSHEET_HOME);
            
      /*
       * DATA DE EXTRAÇÃO
       */
      SimpleDateFormat sdf = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      write(COLUMN_D, ROW_023, styles.get(STYLE_HOME_TEXT), sdf.format(DatesHelper.now()));
      
      /*
       * SUBAREAS
       */
      write(COLUMN_D, ROW_019, styles.get(STYLE_HOME_TEXT), getSubAreasText());
      
      /*
       * TEAMS
       */
      write(COLUMN_D, ROW_021, styles.get(STYLE_HOME_TEXT), getTeamsText());
   }

   @Override
   public void writeContent(ReportView view) {
      for (Team team : this.teams) {
         /*
          * Create worksheet
          */
         this.worksheet = this.workbook.cloneSheet(workbook.getSheetIndex(WORKSHEET_TEMPLATE), getSheetName(team));
         
         /*
          * Write page title
          */
         write(COLUMN_A, ROW_002, styles.get(STYLE_HOME_TITLE), getSheetTitle(team), (float) 45, null);
         
         /*
          * Write table header
          */
         tableHeader();
         
         /*
          * Set initial row
          */
         this.row = ROW_005;
         
         /*
          * Write epics
          */
         epics(team);
         
         /*
          * Write user stories
          */
         userStories(team);
         
         /*
          * Write workitems
          */
         workItems(team);
      }
   }
   
   private void epics(Team team) {
      List<Epic> epics = epicsService.getByTeam(team);
      for (Epic epic : epics) {
         if (epic.getStatus().getId().equals(WorkItemStatus.CANCELED) || epic.getStatus().getId().equals(WorkItemStatus.DUPLICATED))
            continue;
         
         /*
          * Write epic to report
          */
         if (epic.getUserStories() != null && !epic.getUserStories().isEmpty()) {
            userStories(epic.getUserStories());
         }
         else {
            tableRow(getEpic(epic), 
                     getUserStory(epic), 
                     getId(epic), 
                     getWorkItem(epic),
                     getStoryPoints(epic),
                     getEstimate(epic),
                     getHours(epic),
                     getStatus(epic),
                     getInitialDate(epic),
                     getFinalDate(epic),
                     ConstantsHelper.MAPPING_EPICS_UPDATE + ConstantsHelper.MAPPING_SLASH + epic.getId());
         }
      }
   }
   
   private void userStories(Team team) {
      userStories(userStoriesService.getWithoutEpic(team));
   }
   
   private void userStories(List<UserStory> userStories) {
     for (UserStory userStory : userStories) {
        if (userStory.getStatus().getId().equals(WorkItemStatus.CANCELED) || userStory.getStatus().getId().equals(WorkItemStatus.DUPLICATED))
           continue;
        
        if (userStory.getWorkItems() != null && !userStory.getWorkItems().isEmpty()) {
           workItems(userStory.getWorkItems());
        }
        else {
           tableRow(getEpic(userStory), 
                    getUserStory(userStory), 
                    getId(userStory), 
                    getWorkItem(userStory),
                    getStoryPoints(userStory),
                    getEstimate(userStory),
                    getHours(userStory),
                    getStatus(userStory),
                    getInitialDate(userStory),
                    getFinalDate(userStory),
                    ConstantsHelper.MAPPING_USERSTORIES_UPDATE + ConstantsHelper.MAPPING_SLASH + userStory.getId());
        }
     }
   }
   
   private void workItems(Team team) {
      workItems(workItemsService.getWorkItemsWithoutUserStoryByTeam(team));
   }
   
   private void workItems(List<WorkItem> workItems) {
      for (WorkItem workItem : workItems) {
         if (workItem.getStatus().getId().equals(WorkItemStatus.CANCELED) || workItem.getStatus().getId().equals(WorkItemStatus.DUPLICATED))
            continue;
         
         /*
          * Write workitem to report
          */
         tableRow(getEpic(workItem), 
                  getUserStory(workItem), 
                  getId(workItem), 
                  getWorkItem(workItem),
                  getStoryPoints(workItem),
                  getEstimate(workItem),
                  getHours(workItem),
                  getStatus(workItem),
                  getInitialDate(workItem),
                  getFinalDate(workItem),
                  ConstantsHelper.MAPPING_WORKITEMS_UPDATE + ConstantsHelper.MAPPING_SLASH + workItem.getId());
      }
   }
   
   private String getSheetName(Team team) {
      String result = "";
      
      result = team.getName().length() > 20 ? team.getName().substring(0, 20) : team.getName();
      result = result.replace("/", "");
      
      return result;
   }
   
   private String getSheetTitle(Team team) {
      String result = "                              ";
   
      if (team.isTechnicalAssistance()) {
         result += "Manutenção ";
      }
      result += team.getName();
      
      return result.toUpperCase();
   }
   
   private String getEpic(Epic epic) {
      String result = epic.getName();
      
      return result;
   }
   
   private String getUserStory(Epic epic) {
      String result = "";
      
      return result;
   }
   
   private String getId(Epic epic) {
      String result = epic.getNumber();
      
      return result;
   }
   
   private String getWorkItem(Epic epic) {
      String result = "";
      
      return result;
   }
   
   private Integer getStoryPoints(Epic epic) {
      Integer result = epic.getStoryPoints();
      
      return result;
   }
   
   private Integer getEstimate(Epic epic) {
      Integer result = 0;
      
      return result;
   }
   
   private Integer getHours(Epic epic) {
      Integer result = 0;
      
      return result;
   }
   
   private String getStatus(Epic epic) {
      String result = epic.getStatus().getName();
      
      return result;
   }
   
   private Date getInitialDate(Epic epic) {
      Date result = null;
      
      return result;
   }
   
   private Date getFinalDate(Epic epic) {
      Date result = null;
      
      return result;
   }
   
   private String getEpic(UserStory userStory) {
      String result = userStory.getEpic() != null ? userStory.getEpic().getName() : "";

      return result;
   }
   
   private String getUserStory(UserStory userStory) {
      String result = userStory.getName();
      
      return result;
   }
   
   private String getId(UserStory userStory) {
      String result = userStory.getNumber();
      
      return result;
   }
   
   private String getWorkItem(UserStory userStory) {
      String result = "";
      
      return result;
   }
   
   private Integer getStoryPoints(UserStory userStory) {
      Integer result = userStory.getEstimate();
      
      return result;
   }
   
   private Integer getEstimate(UserStory userStory) {
      Integer result = 0;
      
      return result;
   }
   
   private Integer getHours(UserStory userStory) {
      Integer result = 0;
      
      return result;
   }
   
   private String getStatus(UserStory userStory) {
      String result = userStory.getStatus().getName();
      
      return result;
   }
   
   private Date getInitialDate(UserStory userStory) {
      Date result = null;
      
      return result;
   }
   
   private Date getFinalDate(UserStory userStory) {
      Date result = null;
      
      return result;
   }
   
   private String getEpic(WorkItem workItem) {
      String result = workItem.getUserStory() != null && workItem.getUserStory().getEpic() != null ? workItem.getUserStory().getEpic().getName() : "";

      return result;
   }
   
   private String getUserStory(WorkItem workItem) {
      String result = workItem.getUserStory() != null ? workItem.getUserStory().getName() : "";
      
      return result;
   }
   
   private String getId(WorkItem workItem) {
      String result = workItem.getNumber();
      
      return result;
   }
   
   private String getWorkItem(WorkItem workItem) {
      String result = workItem.getName();
      
      return result;
   }
   
   private Integer getStoryPoints(WorkItem workItem) {
      Integer result = workItem.getUserStory() != null ? workItem.getUserStory().getEstimate() : 0;

      return result;
   }
   
   private Integer getEstimate(WorkItem workItem) {
      Integer result = workItem.getEstimate();
      
      return result;
   }
   
   private Integer getHours(WorkItem workItem) {
      Integer result = workItem.getWorkLogs() != null ? workItem.getWorkLogs().stream().mapToInt(WorkItemWorkLog::getTimeSpent).sum() : 0;
      
      return result;
   }
   
   private String getStatus(WorkItem workItem) {
      String result = workItem.getStatus().getName();
      
      return result;
   }
   
   private Date getInitialDate(WorkItem workItem) {
      Date result = null;
      
      if (!workItem.getWorkLogs().isEmpty()) {
         List<WorkItemWorkLog> workLogs = workItem.getWorkLogs();
         
         Collections.sort(workLogs, new Comparator<WorkItemWorkLog>() {
            public int compare(WorkItemWorkLog o1, WorkItemWorkLog o2) {
               if (o1.getDay().getDate() == null || o2.getDay().getDate() == null)
                  return 0;
               return o1.getDay().getDate().compareTo(o2.getDay().getDate());
            }
         });
            
         result = workLogs.get(0).getDay().getDate();
      }
      
      return result;
   }
   
   private Date getFinalDate(WorkItem workItem) {
      Date result = null;

      if (!workItem.getWorkLogs().isEmpty() && workItem.getClosed() != null) {
         result = workItem.getClosed();
      }
      
      return result;
   }
   
   private String getSubAreasText() {
      String result = "";
      
      for (SubArea subArea : this.subAreas) {
         if (result != "")
            result += ", ";
         
         result += subArea.getAbbreviation();
      }
      
      return result;
   }
   
   private String getTeamsText() {
      String result = "";
      
      for (Team team : this.teams) {
         if (result != "")
            result += ", ";
         
         result += team.getName();
      }
      
      return result;
   }
   
   private void tableHeader() {
      write(COLUMN_A, ROW_004, styles.get(STYLE_TABLE_HEADER_ALIGN_LEFT), "EPIC");
      write(COLUMN_B, ROW_004, styles.get(STYLE_TABLE_HEADER_ALIGN_LEFT), "USER STORY");
      write(COLUMN_C, ROW_004, styles.get(STYLE_TABLE_HEADER_ALIGN_CENTER), "ID");
      write(COLUMN_D, ROW_004, styles.get(STYLE_TABLE_HEADER_ALIGN_LEFT), "WORKITEM");
      write(COLUMN_E, ROW_004, styles.get(STYLE_TABLE_HEADER_ALIGN_CENTER), "STORY POINTS");
      write(COLUMN_F, ROW_004, styles.get(STYLE_TABLE_HEADER_ALIGN_CENTER), "ESTIMATIVA");
      write(COLUMN_G, ROW_004, styles.get(STYLE_TABLE_HEADER_ALIGN_CENTER), "HORAS");
      write(COLUMN_H, ROW_004, styles.get(STYLE_TABLE_HEADER_ALIGN_CENTER), "ESTADO");
      write(COLUMN_I, ROW_004, styles.get(STYLE_TABLE_HEADER_ALIGN_CENTER), "DATA INÍCIO");
      write(COLUMN_J, ROW_004, styles.get(STYLE_TABLE_HEADER_ALIGN_CENTER), "DATA FIM");
   }
   
   private void tableRow(String epic, String userStory, String id, String workitem, Integer storyPoints, Integer estimate, Integer hours, String status, Date di, Date df, String url) {
      SimpleDateFormat sdf = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATE);
      
      write(COLUMN_A, this.row, styles.get(STYLE_TABLE_ROW_ALIGN_LEFT), epic);
      write(COLUMN_B, this.row, styles.get(STYLE_TABLE_ROW_ALIGN_LEFT), userStory);
      write(COLUMN_C, this.row, styles.get(STYLE_TABLE_ROW_ALIGN_CENTER), id, null, url);
      write(COLUMN_D, this.row, styles.get(STYLE_TABLE_ROW_ALIGN_LEFT), workitem);
      write(COLUMN_E, this.row, styles.get(STYLE_TABLE_ROW_ALIGN_CENTER), storyPoints);
      write(COLUMN_F, this.row, styles.get(STYLE_TABLE_ROW_ALIGN_CENTER), estimate);
      write(COLUMN_G, this.row, styles.get(STYLE_TABLE_ROW_ALIGN_CENTER), hours);
      write(COLUMN_H, this.row, styles.get(STYLE_TABLE_ROW_ALIGN_CENTER), status);
      write(COLUMN_I, this.row, styles.get(STYLE_TABLE_ROW_ALIGN_CENTER), di != null ? sdf.format(di) : null);
      write(COLUMN_J, this.row, styles.get(STYLE_TABLE_ROW_ALIGN_CENTER), df != null ? sdf.format(df) : null);
      
      this.row++;
   }
}