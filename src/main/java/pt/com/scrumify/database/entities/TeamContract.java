package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_TEAM_CONTRACTS)
@AssociationOverrides({ 
   @AssociationOverride(name = "pk.team", joinColumns = @JoinColumn(name = "team")),
   @AssociationOverride(name = "pk.contract", joinColumns = @JoinColumn(name = "contract")) 
})
public class TeamContract implements Serializable {
   private static final long serialVersionUID = 8209855658816649046L;
   
   @Getter
   @Setter
   @EmbeddedId
   private TeamContractPK pk = new TeamContractPK();
   
   public TeamContract(TeamContractPK pk) {
      super();
      
      this.pk = pk;
   }
      
   @Transient
   public Team getTeam() {
      return this.pk.getTeam();
   }

   public void setTeam(Team team) {
      this.pk.setTeam(team);
   }

   @Transient
   public Contract getContract() {
      return this.pk.getContract();
   }

   public void setContract(Contract contract) {
      this.pk.setContract(contract);
   }
}