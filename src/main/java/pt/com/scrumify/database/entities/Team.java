package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_TEAMS)
public class Team implements Serializable {
   private static final long serialVersionUID = 6877214828924062422L;
   
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Getter
   @Setter
   @Column(name = "id", nullable = false)
   private Integer id = 0;
   
   @Getter
   @Setter
   @Column(name = "name", length = 80, nullable = false)
   private String name;
      
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "subarea", nullable = false)
   private SubArea subArea;
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.team")
   private List<TeamMember> members = new ArrayList<>(0);

   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.team")
   private List<TeamContract> contracts = new ArrayList<>(0);
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "team")
   private List<WorkItem> workItems = new ArrayList<>(0);
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "team")
   private List<Epic> epics = new ArrayList<>(0);
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "team")
   private List<UserStory> userStories = new ArrayList<>(0);
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "team")
   private List<WorkItemHistory> workItemsHistory = new ArrayList<>(0);

   @Getter
   @Setter
   @Column(name = "technicalassistance", nullable = false)
   private boolean technicalAssistance;
   
   @Getter
   @Setter
   @Column(name = "active", nullable = true)
   private boolean active;

   @Getter
   @Setter
   @Column(name = "created", nullable = false)
   private Date created;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "createdby", nullable = false)
   private Resource createdBy;

   @Getter
   @Setter
   @Column(name = "lastupdate", nullable = false)
   private Date lastUpdate;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "lastupdateby", nullable = false)
   private Resource lastUpdateBy;
   
   public Team(Integer id) {
      super();
      this.id = id;
   }
   
   @PrePersist
   public void onInsert() {
      this.created = DatesHelper.now();
      this.createdBy = ResourcesHelper.getResource();
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
      this.active = true;
   }

   @PreUpdate
   public void onUpdate() {
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }
   
   public List<Resource> getTeamMembers(){
      List<Resource> resources = new ArrayList<>();
      for(TeamMember member : this.members)
         resources.add(member.getResource());      
      return resources;
   }
}