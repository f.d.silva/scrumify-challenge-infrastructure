package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.SprintMember;
import pt.com.scrumify.database.entities.SprintMemberPK;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Sprint;

public interface SprintMemberRepository extends JpaRepository<SprintMember, SprintMemberPK> {
   @Query(value = "SELECT sm " +
                  "FROM SprintMember sm " +
                  "WHERE sm.pk.sprint = :sprint " +
                  "ORDER BY sm.pk.resource.name")
   List<SprintMember> getBySprint(@Param("sprint") Sprint sprint);

   List<SprintMember> findByPkResource(Resource resource);
   
}