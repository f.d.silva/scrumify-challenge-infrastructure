package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.TimesheetApprovalMonthly;

public interface TimesheetApprovalMonthlyService {
   List<TimesheetApprovalMonthly> findByContractsAndYear(List<Contract> contracts, int year);
}