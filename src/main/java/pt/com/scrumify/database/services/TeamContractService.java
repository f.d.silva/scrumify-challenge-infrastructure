package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TeamContract;
import pt.com.scrumify.database.entities.TeamContractPK;

public interface TeamContractService {
   void delete(TeamContract teamContract);
   TeamContract getOne(TeamContractPK pk);
   List<TeamContract> getByTeam(Team team);
   TeamContract save(TeamContract teamContract);
}