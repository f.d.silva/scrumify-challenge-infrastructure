package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.services.ActivityService;
import pt.com.scrumify.database.services.ProjectPhaseService;
import pt.com.scrumify.entities.ActivityView;
import pt.com.scrumify.helpers.ConstantsHelper;

@Controller
public class ActivitiesController {
   @Autowired
   private ActivityService activityService;
   @Autowired
   private ProjectPhaseService projectPhaseService;
   
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * ACTIVITIES
    */
   @GetMapping(value = ConstantsHelper.MAPPING_ACTIVITIES)
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_ACTIVITIES_READ + "')")
   public String list(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ACTIVITIES, this.activityService.getAll());
      
      return ConstantsHelper.VIEW_ACTIVITIES_READ;
   }
 
   @GetMapping(value = ConstantsHelper.MAPPING_ACTIVITIES_CREATE)
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_ACTIVITIES_CREATE + "')")
   public String create(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ACTIVITY, new ActivityView());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROJECT_PHASES, projectPhaseService.getAll());

      return ConstantsHelper.VIEW_ACTIVITIES_CREATE;
   }
   
   @GetMapping(value = ConstantsHelper.MAPPING_ACTIVITIES_UPDATE + ConstantsHelper.MAPPING_PARAMETER_ACTIVITY)
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_ACTIVITIES_UPDATE + "')")
   public String update(Model model, @PathVariable int activity) {
      ActivityView view = new ActivityView(this.activityService.getOne(activity));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ACTIVITY, view);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROJECT_PHASES, projectPhaseService.getAll());

      return ConstantsHelper.VIEW_ACTIVITIES_UPDATE;
   }   

   @PostMapping(value = ConstantsHelper.MAPPING_ACTIVITIES_SAVE)
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_ACTIVITIES_UPDATE + "')")
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ACTIVITY) @Valid ActivityView view, BindingResult bindingResult) {
      if (bindingResult.hasErrors()) {
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ACTIVITY, view);
         
         return ConstantsHelper.VIEW_ACTIVITIES_UPDATE;
      }
      
      this.activityService.save(view.translate());

      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_ACTIVITIES;
   }
}