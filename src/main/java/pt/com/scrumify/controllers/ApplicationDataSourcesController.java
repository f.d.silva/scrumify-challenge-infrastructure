package pt.com.scrumify.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.entities.ApplicationDataSource;
import pt.com.scrumify.database.services.ApplicationDataSourceService;
import pt.com.scrumify.entities.ApplicationDataSourceView;
import pt.com.scrumify.helpers.ConstantsHelper;

@Controller
public class ApplicationDataSourcesController {
   @Autowired
   private ApplicationDataSourceService applicationsDataSourceService;
   /*
    * CREATE DATASOURCE IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_DATASOURCE_CREATE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION + ConstantsHelper.MAPPING_PARAMETER_ENVIRONMENT)
   public String create(Model model, @PathVariable int app, @PathVariable int environment) {
      ApplicationDataSourceView view = new ApplicationDataSourceView();
      view.getApplication().setId(app);
      view.getEnvironment().setId(environment);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_DATASOURCE, view);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_DATASOURCE_TYPES, ConstantsHelper.TypesOfDataSource.values());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_DATASOURCE_DATABASE_TYPES, ConstantsHelper.TypesOfDatabase.values());

      return ConstantsHelper.VIEW_APPLICATIONS_DATASOURCE;
   }

   /*
    * UPDATE DATASOURCE IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_DATASOURCE_UPDATE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION + ConstantsHelper.MAPPING_PARAMETER_ENVIRONMENT + ConstantsHelper.MAPPING_PARAMETER_DATASOURCE)
   public String update(Model model, @PathVariable int app, @PathVariable int environment, @PathVariable int datasource) {
      ApplicationDataSource applicationDataSource = applicationsDataSourceService.getOne(datasource, app, environment);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_DATASOURCE_TYPES, ConstantsHelper.TypesOfDataSource.values());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_DATASOURCE_DATABASE_TYPES, ConstantsHelper.TypesOfDatabase.values());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_DATASOURCE, new ApplicationDataSourceView(applicationDataSource));

      return ConstantsHelper.VIEW_APPLICATIONS_DATASOURCE;
   }

   /*
    * SAVE DATASOURCE IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #view.application.id)")
   @PostMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_DATASOURCE_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATION_DATASOURCE) @Valid ApplicationDataSourceView view) {
      this.applicationsDataSourceService.save(view.translate());

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_DATASOURCES, this.applicationsDataSourceService.getByApplicationAndEnvironment(view.getApplication().getId(), view.getEnvironment().getId()));

      return ConstantsHelper.VIEW_FRAGMENT_APPLICATIONS_DATASOURCES;
   }

   /*
    * DELETE DATASOURCE IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_DATASOURCE_DELETE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION + ConstantsHelper.MAPPING_PARAMETER_ENVIRONMENT + ConstantsHelper.MAPPING_PARAMETER_DATASOURCE)
   public String delete(Model model, @PathVariable int app, @PathVariable int environment, @PathVariable int datasource) {
      applicationsDataSourceService.delete(new ApplicationDataSource(datasource));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_DATASOURCES, applicationsDataSourceService.getByApplicationAndEnvironment(app, environment));

      return ConstantsHelper.VIEW_FRAGMENT_APPLICATIONS_DATASOURCES;
   }
}