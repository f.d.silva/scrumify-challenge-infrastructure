package pt.com.scrumify.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.spring5.SpringTemplateEngine;

import io.swagger.annotations.Api;
import pt.com.scrumify.helpers.ConstantsHelper;

@Controller
@Api("Controller to handle all request of javascript templates.")
public class JavascriptsController {
   @Autowired
   private SpringTemplateEngine templateEngine;
   
   @GetMapping(produces = ConstantsHelper.MIME_TYPE_JAVASCRIPT, value = ConstantsHelper.MAPPING_PUBLIC_JAVASCRIPTS + ConstantsHelper.MAPPING_PARAMETER_TEMPLATE)
   public @ResponseBody String jsTemplate(HttpServletRequest request, HttpServletResponse response, @PathVariable String template) {
      WebContext context = new WebContext(request, response, request.getServletContext());
      
      return this.templateEngine.process("javascripts/" + template, context);
   }
}