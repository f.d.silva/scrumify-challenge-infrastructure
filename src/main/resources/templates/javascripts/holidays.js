$(document).ready(function() {
   initialize();
});
        
function initialize() {
	var json = getjson([[@{${T(pt.com.scrumify.helpers.ConstantsHelper).SCRIPT_CALENDAR_LOCALIZATION_JSON}} + ${#locale.language} + '.json']]);
	
	calendar('day', 'date', json,'');
	$('.ui.radio.checkbox').checkbox();
	$('.ui.toggle.checkbox').checkbox();
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
	formValidation();
}

function formSubmission() {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_HOLIDAYS_SAVE}}]];
   var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_HOLIDAYS}}]];
   ajaxpost(url, $("#holidays-form").serialize(), "", false, function() {
      initialize();
   }, returnurl);
}

function formValidation() {
   $('#holidays-form').form({
      on : 'blur',
      inline : true,
      fields : {
    	  day : {
            identifier : 'day',
            rules : [ {
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         },
         name : {
             identifier : 'name',
             rules : [ {
                type : 'empty',
                prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
             } ]
          }         
      },
      onSuccess : function(event) {
         event.preventDefault();
         
         formSubmission();
      }
   });
}

function createHoliday(){
	
	var url =  [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_HOLIDAYS_CREATE}}]] ;
	ajaxget(url,  "div[id='modal']", function() {
        initialize();
    }, true);
}

function generate(id){
	
	var url =  [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_HOLIDAYS_GENERATE}}]] + '/' + id ;
	   
	ajaxget(url, "", function() {           
		initialize();   
	});
}

function editHoliday(id) {
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_HOLIDAYS_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
    
    ajaxget(url, "div[id='modal']", function() {
        initialize();
    }, true);
}

function deleteHoliday(id) {
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_HOLIDAYS_DELETE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
    $("#modal-confirmation")
	.modal({closable: false,
   		onApprove: function() {
   			ajaxget(url, "body", function() {
   	        initialize();
   	       }, false);
   		},
   		onDeny: function() {
   		}
	})
	.modal('show');  
}