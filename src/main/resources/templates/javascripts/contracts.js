$(document).ready(function() {
  var id = $("input[type=hidden][name='id']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_CONTRACTS_CREATE}}]];
	   
  if (id != 0) {
    url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_CONTRACTS} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_UPDATE}]]
  }
	   
  ajaxtab(true, url, false, function(settings) {
    switch(settings.urlData.tab) {
      case "properties":
        getproperties();
        break;  
      case "contract":
      default:		   
        if (id != 0) {
          getcontract();
        }
        break;
    }
  });

  initialize();
});

function initializeIndex() {
  $('.ui.dropdown').dropdown();
  $('select.dropdown').dropdown();
  $('.ui.toggle.checkbox').checkbox();
  $('.ui.checkbox').checkbox();
}
	        
function initialize() {	
  validateDates();
  $('select.dropdown').dropdown();
  $('.ui.toggle.checkbox').checkbox();
  $('.ui.progress').progress();
  $('.ui.dropdown').dropdown();
  $('.ui.checkbox').checkbox();
  $('.user.circle.icon').popup({
    position: 'top center'
  });

  formValidation();
}	

function formSubmission() {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_CONTRACTS_SAVE}}]];
  var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_CONTRACTS}}]];
  
  ajaxpost(url, $("#contracts-form").serialize(), "body", false, function() {
  }, returnurl);
}

function formValidation() {	
  $('#contracts-form').form({
    on : 'blur',
    inline : true,
    fields : {
      name : {
        identifier : 'name',
        rules : [{
          type : 'empty',
          prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      },
      startingDay : {
        identifier : 'startingDay',
        rules : [{
          type : 'empty',
          prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      },
      hours : {
        identifier : 'hours',
        rules : [{
          type : 'empty',
          prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      },
      hours1stYear : {
        identifier : 'hours1stYear',
        rules : [{
          type : 'empty',
          prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      },
      hours2ndYear : {
        identifier : 'hours2ndYear',
        rules : [{
          type : 'empty',
          prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      },
      year : {
        identifier : 'year',
        rules : [{
          type : 'empty',
          prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      },
      status : {
        identifier : 'status',
        rules : [ {
          type : 'empty',
          prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      },
      code : {
        identifier : 'code',
        rules : [ {
          type : 'empty',
          prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      },
      codeRegEx : {
        identifier : 'code',
        rules : [ {
          type : 'regExp',
          prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_PATTERN_NOT_VALID}}]],
          value : '[0-9]{2}-[0-9]{2,2}'
        }]
      }
    },
    onSuccess : function(event) {
      event.preventDefault();
      formSubmission();
    }
  });
}

function getcontract() {
  var id = $("input[type=hidden][name='id']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_CONTRACTS_AJAX} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
   
  ajaxget(url, "div[data-tab='contract']", function() {           
    initialize();
  });
}

function getproperties() {
  var id = $("input[type=hidden][name='id']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_CONTRACTS_PROPERTIES_AJAX} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	   
  ajaxget(url, "div[data-tab='properties']", function() {
    initialize();
  }); 
}

function validateDates() {
  var json = getjson([[@{${T(pt.com.scrumify.helpers.ConstantsHelper).SCRIPT_CALENDAR_LOCALIZATION_JSON}} + ${#locale.language} + '.json']]);	
  var minDate = $("input[type=hidden][id='minDate']").val();
  var maxDate = $("input[type=hidden][id='maxDate']").val();

  calendar('startingDay', 'date', json, minDate, maxDate, 'endingDay');	
}

function filter() {
  var url  = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_CONTRACTS_FILTER}}]];
  
  ajaxget(url, "div[id='modal']", function() {
    $('.ui.checkbox').checkbox();
    $('.ui.toggle.checkbox').checkbox();
    $('#filter-form').form({
      onSuccess : function(event) {
        event.preventDefault();

        ajaxpost(url, $("#filter-form").serialize(), "div[id='contracts']", true, function() {
          initialize();
        });
      }
    });
  }, true);
}