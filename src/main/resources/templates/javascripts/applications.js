$(document).ready(function() {
   var id = $("input[type=hidden][name='id']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_CREATE}}]];
      
   if (id != 0) {
      url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
   }
   
   ajaxtab(true, url, true, function(settings) {   
      var tab = settings.urlData.tab;
      
      switch(tab) {
         case '0':
            if (id != 0) {
               getinfo();
            }
            break;
         case '99':
           getreleases();
           break;
         default:
            getproperties(tab);
      }
   });
  
   initialize();
   
   $("#subArea").change(function() {
      var id = $(this).val();
      var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_SUBAREA_UPDATE}}]] + "/" + id;
      
      ajaxget(url, "div[id='resources']", function() {     
         initialize();   
      });
    });
});

function initialize() {
   $('.ui.radio.checkbox').checkbox();
   $('.ui.toggle.checkbox').checkbox();
   $('.ui.dropdown').dropdown();
   $('select.dropdown').dropdown();
   $('.ui.mini').popup({
      position: 'top center'
   });

   formValidation();
};

function formSubmission() {   
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_SAVE}}]];
   var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS}}]];
      
   ajaxpost(url, $("#app-form").serialize(), "body", false, null, returnurl);
};

function formValidation() {
   $('form').form({
      on : 'blur',
      inline : true,
      fields : {
         name : {
            identifier : 'name',
            rules : [{
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            }]
         },
         taxonomy : {
            identifier : 'taxonomy',
            rules : [{
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            }]
         },
         fqdn : {
            identifier : 'fqdn',
            rules : [{
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            }]
         },
         subarea : {
            identifier : 'subArea',
            rules : [{
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            }]
         },
         type : {
            identifier : 'type',
            rules : [{
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            }]
         },
         responsible : {
            identifier : 'responsible',
            rules : [{
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            }]
         },
         functionalcontact : {
            identifier : 'functionalContact',
            rules : [{
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            }]
         },
         applicationctatus : {
            identifier : 'applicationStatus',
            rules : [{
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            }]
         },
         technicalcontact : {
            identifier : 'technicalContact',
            rules : [{
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            }]
         }
      },               
      onSuccess : function(event) {
         event.preventDefault();
         
         formSubmission();
      }
   });
};

function getinfo() {
   var id = $("input[type=hidden][name='id']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_AJAX} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
   
   ajaxget(url, "div[data-tab='info']", function() {
      initialize();
   });
};

function getproperties(environment) {
   var id = $("input[type=hidden][name='id']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_ENVIRONMENTS_AJAX} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + environment;
   
   ajaxget(url, "div[data-tab='" + environment + "']", function() {
      initialize();
   });
};

function properties(type) {
   var id = $("input[type=hidden][name='appid']").val();
   var environment = window.location.pathname.split("/").pop();
   
   switch(type) {
     case '1':
       addcredential(id, environment);
       break;
     case '2':
       adddatasource(id, environment);
       break;
     case '3':
       addlink(id, environment);
       break;
     case '4':
       addproperty(id, environment);
       break;
    }
};

function addcredential(application, environment) {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_CREDENTIAL_CREATE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + application + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + environment;
   
   credentialrequest(environment, url);
};

function editcredential(application, environment, credential) {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_CREDENTIAL_UPDATE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + application + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + environment + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + credential;
   
   credentialrequest(environment, url);
};

function removecredential(application, environment, credential) {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_CREDENTIAL_DELETE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + application + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + environment + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + credential;

   ajaxdelete(url, "div[id='application-credentials-" + environment + "']", function() {
      initialize();
   }, false);
};

function credentialrequest(environment, url) {
   ajaxget(url, "div[id='modal']", function() {
      $('.ui.radio.checkbox').checkbox();

      $('form').form({
         on: 'blur',
         inline : true,
         fields: { 
            name : {
               identifier : 'name',
               rules : [{
                  type : 'empty',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               }]
            },
            type : {
               identifier : 'type',
               rules : [{
                  type : 'checked',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               }]
            },
            hostname : {
               identifier : 'hostname',
               rules : [{
                  type : 'empty',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               }]
            },
            port : {
               identifier : 'port',
               rules : [{
                  type : 'empty',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               }]
            },
            username : {
               identifier : 'username',
               rules : [{
                  type : 'empty',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               }]
            }
         },
         onSuccess : function(event) {
            event.preventDefault();
            
            url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_CREDENTIAL_SAVE}}]];
                        
            ajaxpost(url, $("#credential-form").serialize(), "div[id='application-credentials-" + environment + "']", true, function() {
               initialize();
            });
         }
      });
   }, true);
};

function adddatasource(application, environment) {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_DATASOURCE_CREATE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + application + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + environment;
   
   datasourcerequest(environment, url);
};

function editdatasource(application, environment, datasource) {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_DATASOURCE_UPDATE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + application + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + environment + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + datasource;
   
   datasourcerequest(environment, url);
};

function removedatasource(application, environment, datasource) {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_DATASOURCE_DELETE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + application + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + environment + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + datasource;
   
   ajaxdelete(url, "div[id='application-datasources-" + environment + "']", function() {
      initialize();
   }, false);
};

function datasourcerequest(environment, url) {
   ajaxget(url, "div[id='modal']", function() {
      $('.ui.radio.checkbox').checkbox();
      $('.ui.toggle.checkbox').checkbox();
      
      $('form').form({
         on: 'blur',
         inline : true,
         fields: { 
            datasourcename : {
               identifier : 'name',
               rules : [{
                  type : 'empty',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               }]
            },
            databasetype : {
               identifier : 'dataBaseType',
               rules : [{
                  type : 'checked',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               }]
            },
            port : {
               identifier : 'port',
               rules : [{
                  type : 'empty',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               }]
            },                
            username : {
               identifier : 'username',
               rules : [{
                  type : 'empty',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
                  }]
            },
            hostname : {
               identifier : 'hostname',
               rules : [{
                  type : 'empty',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               }]
            }
         },
         onSuccess : function(event) {
            event.preventDefault();
            
            url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_DATASOURCE_SAVE}}]];
            
            ajaxpost(url, $("#datasource-form").serialize(), "div[id='application-datasources-" + environment + "']", true, function() {
               initialize();
            });
         }
      });
   }, true);
};

function addlink(application, environment) {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_LINK_CREATE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + application + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + environment;
   
   linkrequest(environment, url);
};

function editlink(application, environment, link) {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_LINK_UPDATE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + application + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + environment + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + link;
   
   linkrequest(environment, url);
};

function removelink(application, environment, link) {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_LINK_DELETE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + application + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + environment + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + link;
   
   ajaxdelete(url, "div[id='application-links-" + environment + "']", function() {
      initialize();
   }, false);
};

function linkrequest(environment, url) {
   ajaxget(url, "div[id='modal']", function() {
      $('form').form({
         on: 'blur',
         inline : true,
         fields: { 
            name : {
               identifier : 'name',
               rules : [{
                  type : 'empty',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               }]
            },
            url : {
               identifier : 'url',
               rules : [{
                  type : 'empty',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               }]
            },
            username : {
               identifier : 'username',
               rules : [{
                  type : 'empty',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               }]
            }
         },
         onSuccess : function(event) {
            event.preventDefault();
            
            url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_LINK_SAVE}}]];
            
            ajaxpost(url, $("#link-form").serialize(), "div[id='application-links-" + environment + "']", true, function() {
               initialize();
            });
         }
      });
   }, true);
};

function addproperty(application, environment) {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_PROPERTY_CREATE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + application + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + environment;
   
   propertyrequest(environment, url);
};

function editproperty(application, environment, property) {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_PROPERTY_UPDATE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + application + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + environment + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + property;
   
   propertyrequest(environment, url);
};

function removeproperty(application, environment, property) {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_PROPERTY_DELETE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + application + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + environment + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + property;

   ajaxdelete(url, "div[id='application-properties-" + environment + "']", function() {
      initialize();
   }, false);
};

function propertyrequest(environment, url) {
   ajaxget(url, "div[id='modal']", function() {
      $('ui.dropdown').dropdown();
      $('select.dropdown').dropdown();
      
      $('form').form({
         on: 'blur',
         inline : true,
         fields: { 
            value : {
               identifier : 'value',
               rules : [{
                  type : 'empty',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               }]
            },
            propertyGroup : {
               identifier : 'pk.property.id',
               rules : [{
                  type : 'empty',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               }]
            }
         },
         onSuccess : function(event) {
            event.preventDefault();
            
            url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_PROPERTY_SAVE}}]];
                        
            ajaxpost(url, $("#property-form").serialize(), "div[id='application-properties-" + environment + "']", true, function() {
               initialize();
            });
         }
      });
   }, true);
};

function getreleases() {
  var id = $("input[type=hidden][name='appid']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPLICATIONS_RELEASES_AJAX} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
  
  ajaxget(url, "div[data-tab='99']", function() {
     initialize();
  });
};

function addrelease() {
  var application = $("input[type=hidden][name='appid']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_RELEASE_CREATE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + application;
   
  releaserequest(url);
};

function editrelease(release) {
  var application = $("input[type=hidden][name='appid']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_RELEASE_UPDATE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + application + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + release;
   
  releaserequest(url);
};

function removerelease(version) {
	var application = $("input[type=hidden][name='appid']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_RELEASE_DELETE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + application + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + version;

   ajaxdelete(url, "div[data-tab='99']", function() {
      initialize();
   }, false);
};

function releaserequest(url) {
   ajaxget(url, "div[id='modal']", function() {
      $('ui.dropdown').dropdown();
      $('select.dropdown').dropdown();
      
      $('form').form({
         on: 'blur',
         inline : true,
         fields: { 
            value : {
               identifier : 'version',
               rules : [{
                  type : 'empty',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               }]
            },
            propertyGroup : {
               identifier : 'major',
               rules : [{
                  type : 'empty',
                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               }]
            }
         },
         onSuccess : function(event) {
            event.preventDefault();
            
            url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_RELEASE_SAVE}}]];
            
            ajaxpost(url, $("#version-form").serialize(), "div[data-tab='99']", true, function() {
               initialize();
            });
         }
      });
   }, true);
};

function checkDataBaseType() {
   var dbtype = $('input[name=datasourceDataBaseType]:checked').val();
   if (dbtype == 1) {
      var from = jQuery("input[name='datasourceType']");
      from.attr('disabled', 'disabled');
      $("#datasourceType1").prop("checked", false);
      $("#datasourceType2").prop("checked", false);
   } else {
      var from = jQuery("input[name='datasourceType']");
      from.attr('disabled', false);
      $("#datasourceType1").prop("checked", 'checked');
   }
}